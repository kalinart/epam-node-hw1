const express = require('express');
const morgan = require('morgan');
const PORT = 8080;
const fileRouter = require('./routes/filesRouter');

const app = express();

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/files', fileRouter);

app.listen(PORT, () => {
  console.log(`Server has been started on port ${PORT}`);
});
