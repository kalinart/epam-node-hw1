const fs = require('fs').promises;
const path = require('path');
const filesExtantions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

function isFileValid(file) {
  const extension = file.split('.').pop();
  return filesExtantions.includes(extension);
}

// GET /api/files
const getFiles = async (req, res) => {
  const filesPath = path.resolve('files');
  try {
    const files = await fs.readdir(filesPath);
    res.json({
      message: 'Success',
      files
    });
  } catch (error) {
    // If dir does not exist
    if (error.code === 'ENOENT') {
      return res.status(400).json({
        message: 'Client error'
      });
    }
    res.status(500).json({ message: 'Server error' });
  }
};

// POST /api/files
const createFile = async (req, res) => {
  const { filename, content } = req.body;

  if (!content) {
    return res
      .status(400)
      .json({ message: `Please specify 'content' parameter` });
  }

  if (!filename) {
    return res
      .status(400)
      .json({ message: `Please specify 'filename' parameter` });
  }

  if (!isFileValid(filename)) {
    const correctExtentions = filesExtantions.join(', ');
    return res.status(400).json({
      message: `The file's extention should be: ${correctExtentions}`
    });
  }

  // Create dir for files
  const filesDir = path.resolve('files');
  try {
    await fs.mkdir(filesDir);
  } catch (error) {
    // console.log(error);
  }

  // create path to file
  const filePath = path.join(filesDir, filename);

  try {
    await fs.writeFile(filePath, content, 'utf8');
    res.json({
      message: 'File created successfully'
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: 'Server error' });
  }
};

// GET /api/files/:filename
const getFileByFilename = async (req, res) => {
  const { filename } = req.params;
  const filePath = path.resolve(path.join('files/', filename));
  const extension = path.extname(filename).slice(1);

  try {
    const content = await fs.readFile(filePath, 'utf8');
    const { birthtime } = await fs.stat(filePath);

    res.json({
      message: 'Success',
      filename,
      content,
      extension,
      uploadedDate: birthtime
    });
  } catch (error) {
    if (error.code === 'ENOENT') {
      return res.status(400).json({
        message: `No file with '${filename}' found`
      });
    }

    res.status(500).json({
      message: 'Server error'
    });
  }
};

module.exports = {
  getFiles,
  createFile,
  getFileByFilename
};
